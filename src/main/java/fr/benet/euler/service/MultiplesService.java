package fr.benet.euler.service;

import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

/**
 * This service declares methods to solve the first problem of the website https://projecteuler.net/
 */
@Component
public class MultiplesService {

    /**
     * This method returns the answer of the first problem of the website https://projecteuler.net/.
     *
     * @return the answer, which is 233168
     */
    public String getAnswer() {
        Integer result = IntStream.range(1, 1000)
                .boxed()
                .toList()
                .stream()
                .filter(i -> i % 3 == 0 || i % 5 == 0)
                .reduce(0, Integer::sum);

        return String.valueOf(result);
    }
}
