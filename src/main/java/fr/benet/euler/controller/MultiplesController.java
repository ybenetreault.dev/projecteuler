package fr.benet.euler.controller;

import fr.benet.euler.service.MultiplesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller exposes endpoints to answer the first problem of the website https://projecteuler.net/
 */
@RestController
public class MultiplesController {

    // === Attributs ===
    private MultiplesService multiplesService;

    // === Constructors ===
    public MultiplesController(MultiplesService multiplesService) {
        this.multiplesService = multiplesService;
    }

    // === Methods ===

    /**
     * This endpoint gives the answer for the initial problem.
     *
     * @return the answer, which is 233168
     */
    @GetMapping("/multiples/answer")
    public String getAnswer() {
        return multiplesService.getAnswer();
    }
}
