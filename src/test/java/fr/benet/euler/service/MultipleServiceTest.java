package fr.benet.euler.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MultipleServiceTest {

    @Autowired
    MultiplesService multiplesService;

    @Test
    public void getAnswerTest() {
        String answer = multiplesService.getAnswer();

        Assertions.assertEquals("233168", answer);
    }
}
